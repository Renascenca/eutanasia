Este trabalho � uma proposta de visualiza��o comparada da legisla��o da eutan�sia e suic�dio assistido nos pa�ses em an�lise. Todos os crit�rios e classifica��es foram desenvolvidos em colabora��o com um constitucionalista e uma especialista em bio�tica e �tica m�dica.
Dois eixos guiam esta an�lise de dados: liberaliza��o (eixo horizontal) e regulamenta��o (eixo vertical).
No eixo horizontal foram definidos quatro n�veis - proibi��o total (0), eutan�sia legal com condicionamentos e apenas aplic�vel em alguns territ�rios e prov�ncias (0.4) eutan�sia legal com condicionamentos (0.8) legaliza��o total (1).
No eixo vertical, os crit�rios aplicados para obter o grau de regulamenta��o foram: participa��o m�dica, natureza do parecer - consultivo ou vinculativo, registo m�dico, acesso a cuidados paliativos, vontade (atual ou anterior), condi��o na origem do pedido e idade m�nima de requerimento.
F�rmula�do eixo vertical

(20%) Participa��o m�dica - obrigatoriedade (50%), n�mero de profissionais (30%) , presen�a no acto (20%)�

obrigatoriedade: 0 - n�o |� 1 - sim
n�mero de profissionais: 0 - menos de dois ou dois� |   1 - mais de dois
presen�a de um m�dico no ato: 0 - n�o� |   1 - sim��

(10%) Natureza do parecer - vinculativo ou consultivo�
0 - consultivo
1 - vinculativo�

(10%) Registo m�dico - se � ou n�o registada a informa��o do m�dico que participou no processo
0 - n�o
1 - sim�

Nota: Registo m�dico no luxemburgo � feito, mas s� � consultado em caso de duvida por parte da comiss�o nacional de controlo e avalia��o em rela��o aos procedimentos�
"Todas as solicita��es feitas pelo paciente, bem como as medidas tomadas pelo m�dico assistente e seu resultado, incluindo o (s) relat�rio (s) do (s) m�dico (s) consultado (s), s�o regularmente registradas no arquivo m�dico do paciente."
"O m�dico que pratica eutan�sia ou suic�dio assistido deve enviar, dentro de oito dias, o documento de registro referido no artigo 7, devidamente preenchido, � Comiss�o Nacional de Controle e Avalia��o a que se refere o artigo 6. desta lei"
Parece que s� s�o confidenciais alguns dados do m�dico (morada, contacto�)

(10%) Registo de acesso a cuidados paliativos - sim ou n�o�
N�o - 0
Sim - 1

(20%) Vontade - atual ou anterior�
Anterior - 0
Atual - 1

(20%) Condi��o na origem do pedido - s� fase terminal, doen�a incur�vel, dem�ncia e doen�a mental
sofrimento f�sico ou ps�quico - 0�
sofrimento f�sico ou ps�quico insuport�vel por causa de doen�a incur�vel - 1
(Sem perspectiva de melhoria - incur�vel?)�

(10%) Idade - menores (sim ou n�o)
Sim - 0
N�o - 1

Com base no c�lculo dos valores dos dois eixos, obtivemos uma representa��o gr�fica das cinco legisla��es que permite avaliar os n�veis de liberaliza��o e regulamenta��o dos respetivos regimes jur�dicos e compar�-los nestes dois aspetos fundamentais.
Fontes: 
1. http://www.ejustice.just.fgov.be/cgi_loi/change_lg.pl?language=fr&la=F&cn=2002052837&table_name=loi
2. http://legilux.public.lu/eli/etat/leg/loi/2009/03/16/n2/jo
3. http://ficheiros.parlamento.pt/DILP/Dossiers_informacao/Eutanasia/Holanda_Ley_2002.pdf
4. https://www.canada.ca/en/health-canada/services/medical-assistance-dying.html#a8
5. http://www.legislation.vic.gov.au/Domino/Web_Notes/LDMS/PubStatbook.nsf/f932b66241ecf1b7ca256e92000e23be/B320E209775D253CCA2581ED00114C60/$FILE/17-061aa%20authorised.pdf
6. http://app.parlamento.pt/webutils/docs/doc.pdf?path=6148523063446f764c324679595842774f6a63334e7a637664326c756157357059326c6864476c3259584d7657456c574c33526c6548527663793977616d77324e793159535659755a47396a&fich=pjl67-XIV.doc&Inline=true
7. http://app.parlamento.pt/webutils/docs/doc.pdf?path=6148523063446f764c324679595842774f6a63334e7a637664326c756157357059326c6864476c3259584d7657456c574c33526c6548527663793977616d77304c56684a5669356b62324d3d&fich=pjl4-XIV.doc&Inline=true
8. http://app.parlamento.pt/webutils/docs/doc.pdf?path=6148523063446f764c324679595842774f6a63334e7a637664326c756157357059326c6864476c3259584d7657456c574c33526c6548527663793977616d77784d44517457456c574c6d527659773d3d&fich=pjl104-XIV.doc&Inline=true
9. http://app.parlamento.pt/webutils/docs/doc.pdf?path=6148523063446f764c324679595842774f6a63334e7a637664326c756157357059326c6864476c3259584d7657456c574c33526c6548527663793977616d77784e6a677457456c574c6d527659773d3d&fich=pjl168-XIV.doc&Inline=true
10. http://app.parlamento.pt/webutils/docs/doc.pdf?path=6148523063446f764c324679595842774f6a63334e7a637664326c756157357059326c6864476c3259584d7657456c574c33526c6548527663793977616d77784f54557457456c574c6d527659773d3d&fich=pjl195-XIV.doc&Inline=true
